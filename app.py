from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello():
    return '<h1>Hi, my name is Elias, Nice to meet to!!</h1>'


if __name__ == "__main__":
    app.run(debug=True)
